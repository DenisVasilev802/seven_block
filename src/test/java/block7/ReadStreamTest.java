package block7;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.configuration.IMockitoConfiguration;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;


import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

public class ReadStreamTest {

    private final static int ARRAY_SIZE = 21;
    int[] intArray;
    ReadStream readStream;

    @Before
    public void setUp() {
        intArray = new int[ARRAY_SIZE];
        for (int i = 0; i < ARRAY_SIZE; i++) {
            intArray[i] = i;
        }
        readStream = new ReadStream();
    }

    @Test
    public void readAnWriteBytesTest() {
        String origin = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]";
        assertEquals(origin, readStream.readAnWriteBytes(intArray, ARRAY_SIZE).toString());

    }

    @Test
    public void readAndWriteCharsTest() {
        String origin = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]";
        assertEquals(origin, readStream.readAndWriteChars(intArray, ARRAY_SIZE).toString());

    }

    @Test
    public void readFromCertainPositionTest() throws IOException {
        File file = new File("file.txt");

        String origin = "[ 4 5 6 7 8 9 10]";


        assertEquals(origin, readStream.readFromCertainPosition(file).toString());

    }

    @Test
    public void getAllFileInFileTreeTest() throws IOException {
        String origin = "block7                          Directory  ./src/test/java/block7\n" +
                "buildinginfo                    Directory  ./src/test/java/block7/buildinginfo\n" +
                "objects                         Directory  ./src/test/java/block7/buildinginfo/objects\n" +
                "PersonTest.java                 File  ./src/test/java/block7/buildinginfo/objects/PersonTest.java\n" +
                "SerializableServiceTest.java    File  ./src/test/java/block7/buildinginfo/SerializableServiceTest.java\n" +
                "SerializableJsonTest.java       File  ./src/test/java/block7/buildinginfo/SerializableJsonTest.java\n" +
                "ReadStreamTest.java             File  ./src/test/java/block7/ReadStreamTest.java\n";
        FileSystem fs = Mockito.mock(FileSystem.class);
        String fileName = "./src/test/java/block7/";
        FileSystem fss = FileSystems.getDefault();
        Path path = fss.getPath(fileName);
        GetAllFileInFileTree getAllFileInFileTree = new GetAllFileInFileTree(fileName);
        when(fs.getPath(fileName)).thenReturn(path);

        assertEquals(getAllFileInFileTree.getAllFileInTree(), origin);
    }


}
