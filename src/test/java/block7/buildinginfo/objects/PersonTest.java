package block7.buildinginfo.objects;

import junit.framework.TestCase;
import org.junit.Test;
import org.mockito.Mockito;

public class PersonTest extends TestCase {
    @Test
    public void testTestGetName() {
        Person person= Mockito.mock(Person.class);
        Person[]people={person,person};
        Flat flat = new Flat(1,2,people);
        Mockito.when(person.getName()).thenReturn("good");
        assertEquals("good",flat.getPeople()[0].getName());
    }
}