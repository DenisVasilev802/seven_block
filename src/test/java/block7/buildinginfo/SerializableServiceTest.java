package block7.buildinginfo;

import block7.buildinginfo.objects.Flat;
import block7.buildinginfo.objects.House;
import block7.buildinginfo.objects.Person;
import junit.framework.TestCase;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class SerializableServiceTest extends TestCase {
    @Test
    public void testDeserialization() throws IOException, ClassNotFoundException {
        SerializableService serializableService = new SerializableService();
        Person person = new Person("Bob", "Black", "Blackov", "01.02.2000");
        Person[] people = {person, person, person};
        Flat flat = new Flat(1, 25, people);
        ArrayList<Flat> flats = new ArrayList<>();
        flats.add(flat);
        flats.add(flat);
        flats.add(flat);

        House house = new House(152, "Omskay 152", person, flats);


        assertEquals(house, serializableService.deserialization("temp.out"));

    }
}