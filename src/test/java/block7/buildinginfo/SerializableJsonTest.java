package block7.buildinginfo;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class SerializableJsonTest extends TestCase {
    @Test
    public void testDeserialization() {
        String origin = "{house={number=152, address=Omskay 152, ownerHouse={name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, flats=[{number=1, area=25, people=[{name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, {name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, {name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}]}, {number=1, area=25, people=[{name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, {name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, {name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}]}, {number=1, area=25, people=[{name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, {name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}, {name=Bob, surname=Black, patronymic=Blackov, dateBirth=01.02.2000}]}]}}";
        SerializableJson serializableJson = new SerializableJson();
        assertEquals(serializableJson.deserialization(), origin);

    }

    @Test
    public void testEquals() {
        SerializableJson serializableJson = new SerializableJson();
        assertTrue(serializableJson.equalsJsonString("./house.json", "./house2.json"));
    }
}