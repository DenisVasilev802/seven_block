package block7.buildinginfo;

import block7.buildinginfo.objects.Flat;
import block7.buildinginfo.objects.House;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SerializableJson {
    Map<String, Object> houseMap;
    ObjectMapper mapper;

    public void serialization(House house) {
        mapper = new ObjectMapper();
        houseMap = new HashMap<>();
        houseMap.put("house", house);
        try {
            mapper.writeValue(new File("./house2.json"), houseMap);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String deserialization() {
        mapper = new ObjectMapper();
        try {
            houseMap = mapper.readValue(new File("./house.json"), Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(houseMap);
        return houseMap.toString();

    }

    public boolean equalsJsonString(String fileName, String fileName2) {
        mapper = new ObjectMapper();
        try {
            if (mapper.readTree(new File(fileName)).equals(
                    mapper.readTree(new File(fileName2)))) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;

    }
}
