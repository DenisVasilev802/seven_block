package block7.buildinginfo;

import block7.buildinginfo.objects.Flat;
import block7.buildinginfo.objects.House;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class SerializableService {
    private static final String CSV_SEPARATOR = ",";

    public SerializableService() {

    }

    public void serialization(House house) throws IOException {
        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(house);
        oos.flush();
        oos.close();
    }

    public House deserialization(String fileName) throws IOException {
        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream oin = new ObjectInputStream(fis);
        House house = null;
        try {
            house = (House) oin.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return house;
    }

    public void writeToCSV(House house) {
        String fileName = String.format("house_%s.csv", house.getNumber());
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));
            StringBuffer oneLine = new StringBuffer();
            oneLine.append("information about house,,\n");
            oneLine.append(String.format("house number:,   %s,\n", house.getNumber()));
            oneLine.append(String.format("house address:,   %s,\n", house.getAddress()));
            oneLine.append(String.format("house owner:,   %s,\n", house.getOwnerHouse()));
            oneLine.append("information about flats,,,\n");
            for (Flat flat : house.getFlats()) {
                oneLine.append(flat.toString());
                oneLine.append("\n");
            }
            bw.write(oneLine.toString());
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
