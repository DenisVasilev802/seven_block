package block7.buildinginfo.objects;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable {
    private String name;
    private String surname;
    private String patronymic;

    @Override
    public String toString() {
        return "" + name +
                "." + surname.charAt(0)+
                "." + patronymic.charAt(0);
    }

    public Person(String name, String surname, String patronymic, String dateBirth) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.dateBirth = dateBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(patronymic, person.patronymic) &&
                Objects.equals(dateBirth, person.dateBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, dateBirth);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    private String dateBirth;
}

