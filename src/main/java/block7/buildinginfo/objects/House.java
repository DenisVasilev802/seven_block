package block7.buildinginfo.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class House implements Serializable {
    private int number;
    private String address;
    private Person ownerHouse;
    private ArrayList<Flat> flats;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return number == house.number &&
                Objects.equals(address, house.address) &&
                Objects.equals(ownerHouse, house.ownerHouse) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, address, ownerHouse, flats);
    }

    public House(int number, String address, Person ownerHouse, ArrayList<Flat> flats) {
        this.number = number;
        this.address = address;
        this.ownerHouse = ownerHouse;
        this.flats = flats;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getOwnerHouse() {
        return ownerHouse;
    }

    public void setOwnerHouse(Person ownerHouse) {
        this.ownerHouse = ownerHouse;
    }

    public ArrayList<Flat> getFlats() {
        return flats;
    }

    public void setFlats(ArrayList<Flat> flats) {
        this.flats = flats;
    }
}
