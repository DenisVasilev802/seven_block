package block7.buildinginfo.objects;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Flat implements Serializable {
    private int number;
    private int area;
    private Person[] people;

    @Override
    public String toString() {
        return "" + number +
                "," + area +
                "," + Arrays.toString(people)
                .replace(",", ";")
                .replace("[", "")
                .replace("]", "")
                .replace(" ","");
    }

    public Flat(int number, int area, Person[] people) {
        this.number = number;
        this.area = area;
        this.people = people;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                area == flat.area &&
                Arrays.equals(people, flat.people);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(number, area);
        result = 31 * result + Arrays.hashCode(people);
        return result;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public Person[] getPeople() {
        return people;
    }

    public void setPeople(Person[] people) {
        this.people = people;
    }
}
