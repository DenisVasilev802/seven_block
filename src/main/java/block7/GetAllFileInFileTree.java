package block7;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.spi.FileSystemProvider;
import java.util.stream.Stream;

/**
 * Task 4/5
 * This class get information about files and directories and write it in file
 * (Пояснение) "Я решил использовать nio, потому что как я знаю сдесь удобнее реализованно прохождение по всему дереву файлов
 * ( и я просто лучше знаю как тут делать)так как в задании не было сказанно, что нужно использовать обязательно io,
 * я подумал что так можно."
 */
public class GetAllFileInFileTree {
    private static final int MAXSIZE = 30;

    private Path path;
    String result;
    StringBuilder stringBuffer;
    String fileName;


    /**
     * Get all files in file tree
     *
     * @throws IOException exception
     */
    public GetAllFileInFileTree(String fileName) throws IOException {
        this.fileName = fileName;


    }

    public String getAllFileInTree() throws IOException {
        FileSystem fs = FileSystems.getDefault();
        path = fs.getPath(fileName);
        stringBuffer = new StringBuilder();
        try (Stream<Path> pathStream = Files.walk(path)) {
            pathStream.forEach(this::writeFile);
        }
        stringBuffer.reverse();
        return result;
    }

    /**
     * Write file info in file
     *
     * @param filePath path file
     */
    void writeFile(final Path filePath) {
        try {
            writeFileInfo(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Write information about files : name,type,path
     *
     * @param filePath file path
     * @throws IOException exception
     */
    void writeFileInfo(final Path filePath) throws IOException {

        stringBuffer.append(getFileName(filePath.getFileName()));
        if (Files.isDirectory(filePath)) {
            stringBuffer.append("Directory  ");
        } else if (Files.isRegularFile(filePath)) {
            stringBuffer.append("File  ");
        }
        stringBuffer.append(filePath);
        stringBuffer.append('\n');
        result = stringBuffer.toString();
    }

    /**
     * Set name of file and set indent
     *
     * @param fileName file path
     * @return sting name
     */
    private String getFileName(final Path fileName) {
        int nameLength = fileName.toString().length();
        if (nameLength < MAXSIZE) {
            StringBuilder stringOfSpace = new StringBuilder();
            for (int i = 0; i < MAXSIZE - nameLength; i++) {
                stringOfSpace.append(" ");
            }

            return fileName.toString() + stringOfSpace.toString() + "  ";
        } else {
            return fileName.toString();
        }

    }
}