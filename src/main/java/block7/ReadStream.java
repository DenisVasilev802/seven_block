package block7;


import java.io.*;
import java.util.ArrayList;

public class ReadStream {
    private final static int ARRAY_SIZE = 21;
    private BufferedReader bufferedReader;
    private int out = 0;

    /**
     * 1 task
     *
     * @param intArray
     * @param arraySize
     */
    public ArrayList<Integer> readAnWriteBytes(int[] intArray, int arraySize) {

        bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(convertIntToByte(intArray),
                0, arraySize)), arraySize);
        ArrayList<Integer> outList = read(bufferedReader);
        close(bufferedReader);

        return outList;
    }

    /**
     * 2 task
     *
     * @param intArray
     * @param arraySize
     */
    public ArrayList<Integer> readAndWriteChars(int[] intArray, int arraySize) {
        bufferedReader = new BufferedReader(new CharArrayReader(convertIntToChar(intArray)));
        ArrayList<Integer> outList = read(bufferedReader);
        read(bufferedReader);
        close(bufferedReader);
        return outList;
    }

    private ArrayList<Integer> read(BufferedReader reader) {
        ArrayList<Integer> listOut = new ArrayList<>();
        do {
            try {
                out = reader.read();
                if (out != -1) {
                    listOut.add(out);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (out != -1);
        return listOut;
    }

    private void close(BufferedReader bufferedReader) {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] convertIntToByte(int[] intArray) {
        byte[] buf = new byte[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            buf[i] = (byte) intArray[i];
        }
        return buf;
    }

    private char[] convertIntToChar(int[] intArray) {
        char[] charArray = new char[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            charArray[i] = (char) intArray[i];
        }
        return charArray;
    }

    /**
     * 3 task
     */
    public ArrayList<String> readFromCertainPosition(File file) {
        RandomAccessFile raf = null;
        ArrayList<String> listOut = new ArrayList<>();

        try {
            raf = new RandomAccessFile(file, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            raf.seek(5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            listOut.add(raf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOut;
    }

}
