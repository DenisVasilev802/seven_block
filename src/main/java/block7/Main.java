package block7;

import block7.buildinginfo.SerializableJson;
import block7.buildinginfo.objects.Flat;
import block7.buildinginfo.objects.House;
import block7.buildinginfo.objects.Person;
import block7.buildinginfo.SerializableService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    private final static int ARRAY_SIZE = 21;

    public static void main(String[] args) throws IOException {
        File file = new File("file.txt");
        int[] intArray = new int[ARRAY_SIZE];
        for (int i = 0; i < ARRAY_SIZE; i++) {
            intArray[i] = i;
        }
        ReadStream readStream = new ReadStream();
        String fileName = "./src";
        GetAllFileInFileTree getAllFileInFileTree = new GetAllFileInFileTree(fileName);
        System.out.println(getAllFileInFileTree.getAllFileInTree());
        readStream.readAnWriteBytes(intArray, ARRAY_SIZE);
        readStream.readAndWriteChars(intArray, ARRAY_SIZE);
        System.out.println(readStream.readFromCertainPosition(file));

        Person person = new Person("Bob", "Black", "Blackov", "01.02.2000");
        Person[] people = {person, person, person};
        Flat flat = new Flat(1, 25, people);
        ArrayList<Flat> flats = new ArrayList<>();
        flats.add(flat);
        flats.add(flat);
        flats.add(flat);

        House house = new House(152, "Omskay 152", person, flats);
        SerializableService serializableService = new SerializableService();
        serializableService.serialization(house);
        serializableService.deserialization("temp.out");
        serializableService.writeToCSV(house);

        SerializableJson serializableJson = new SerializableJson();
        serializableJson.serialization(house);
        serializableJson.deserialization();
        System.out.println(serializableJson.equalsJsonString("./house.json", "./house2.json"));


    }
}
